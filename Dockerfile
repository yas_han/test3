FROM ubuntu:20.04
RUN apt-get -y update && apt-get -y upgrade
RUN apt-get install -y python3 python3-pip curl 
RUN pip3 install Flask
ADD ./hwserver.py /home/hwserver.py
WORKDIR /home
CMD ["python3", "/home/hwserver.py"]
