#!/usr/bin/env python

from flask import Flask     # importe les librairies Flask, pip install flask
app = Flask(__name__)       # crée le nouveau site dans la variable app

@app.route("/")
def hello():
    return "Hello World!"

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=int("5000"))